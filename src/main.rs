use futures_util::{SinkExt, StreamExt, TryFutureExt};
use rust_fuzzy_search::fuzzy_compare;
use serde_derive::{Deserialize, Serialize};
use std::{
    collections::HashMap,
    sync::{
        atomic::{AtomicUsize, Ordering},
        Arc,
    },
};
use str_sim::sim_jaro;
use strsim::normalized_levenshtein;
use tokio::sync::{mpsc, RwLock};
use tokio_stream::wrappers::UnboundedReceiverStream;
use warp::{
    ws::{Message, WebSocket},
    Filter,
};

static NEXT_ID: AtomicUsize = AtomicUsize::new(1);

type Connections = Arc<RwLock<HashMap<usize, mpsc::UnboundedSender<Message>>>>;

#[derive(Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
struct SimilarityRequest {
    strings: [String; 2],
}

impl SimilarityRequest {
    async fn compute_similarity(self, connections: &Connections) {
        let mut response: SimilarityResponse = Default::default();
        response.similarity_a =
            normalized_levenshtein(&self.strings[0], &self.strings[1]).abs() as f32;
        response.similarity_b = fuzzy_compare(&self.strings[0], &self.strings[1]);
        response.similarity_c = sim_jaro(&self.strings[0], &self.strings[1]) as f32;
        response.strings = self.strings;

        let msg = Message::text(serde_json::to_string(&response).unwrap()); //unwrap is ok because we construct struct localy

        for (_, tx) in connections.read().await.iter() {
            if let Err(_disconnected) = tx.send(msg.clone()) {}
        }
    }
}
#[derive(Serialize, Debug, Default)]
#[serde(rename_all = "camelCase")]
struct SimilarityResponse {
    strings: [String; 2],
    similarity_a: f32,
    similarity_b: f32,
    similarity_c: f32,
}

fn main() {
    let runtime = tokio::runtime::Runtime::new().unwrap();
    runtime.block_on(async {
        let connections = Connections::default();

        let connections = warp::any().map(move || connections.clone());

        let similarity =
            warp::any()
                .and(warp::ws())
                .and(connections)
                .map(|ws: warp::ws::Ws, connections| {
                    ws.on_upgrade(move |socket| handle_client(socket, connections))
                });

        warp::serve(similarity).run(([127, 0, 0, 1], 8000)).await;
    })
}

async fn handle_client(ws: WebSocket, connections: Connections) {
    let id = NEXT_ID.fetch_add(1, Ordering::Relaxed);

    let (mut ws_transmit, mut ws_recived) = ws.split();

    let (tx, rx) = mpsc::unbounded_channel();
    let mut rx = UnboundedReceiverStream::new(rx);

    tokio::task::spawn(async move {
        while let Some(response) = rx.next().await {
            ws_transmit
                .send(response)
                .unwrap_or_else(|e| {
                    eprintln!("websocket send error: {}", e);
                })
                .await;
        }
    });

    connections.write().await.insert(id, tx);

    while let Some(result) = ws_recived.next().await {
        let msg = match result {
            Ok(msg) => msg,
            Err(e) => {
                println!("error occured: {}, on client {}", e, id);
                break;
            }
        };
        match validate_request(msg) {
            Ok(request) => {
                request.compute_similarity(&connections).await;
                ()
            }
            Err(e) => {
                respond_sender(id, e, &connections).await;
                ()
            }
        };
    }
    cleanup_client(id, &connections).await;
}

async fn respond_sender(id: usize, response: &str, connections: &Connections) {
    if let Err(_disconnected) = connections
        .read()
        .await
        .get(&id)
        .unwrap() //unwrap is ok because we never remove or change chanell end associated with id
        .send(Message::text(response.clone()))
    {}
}
fn validate_request(msg: Message) -> Result<SimilarityRequest, &'static str> {
    let msg = if let Ok(s) = msg.to_str() {
        s
    } else {
        return Err("Only text messages are allowed");
    };

    let similarity_request: SimilarityRequest = match serde_json::from_str(msg) {
        Ok(r) => r,
        Err(_e) => return Err("Failed to serialize request - wrong format"),
    };

    Ok(similarity_request)
}

async fn cleanup_client(id: usize, connections: &Connections) {
    connections.write().await.remove(&id);
}
