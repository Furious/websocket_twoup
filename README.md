Zadanie rekrutacyjne na stanowisko BE debelopera w Two Up Digital.

Backend przymmuje połączenia pod adresem 127.0.0.1 na porcie 8000 na dowolnym endpoint'cie.

Backend dla klientów websocket przyjmujący 2 elementową listę stringów w formacie JSON jak w poniższym przykładzie:

{
    "strings": ["pierwszy przykładowy string ", "drugi przykładowy string"]
}

W odpowiedzi, backend zwraca wszystkim podłaczonym klientom tekst w formacie JSON zawierający listę stringów otrzymaną od klienta oraz 3 wskaźniki poziomu podobieństwa tychże stringów w postaci ułamka dziesiętnego w zakresie 0 - 1 o dokładności float32.

{
    "strings": [
        "pierwszy przykładowy string ",
        "drugi przykładowy string"
    ],
    "similarityA": 0.71428573,
    "similarityB": 0.62068963,
    "similarityC": 0.7718254
}

Podobieństa obliczono następującymi metodami:
- similarityA - znormalizowany algorytm Levenshtein'a
- similarityB - logiką rozmytą z pomocą trigramami
- similarityC - dystansu Matthew A. Jaro

W przypadku otrzymania od klienta zapytania o formacie niezgodym z oczekiwanym, klient ten otrzyma informację zwrotną:
"Failed to serialize request - wrong format"

W przypadku otrzymania od klienta zapytania w postaci binarnej, klient ten otrzyma informację zwrotną:
"Only text messages are allowed"
